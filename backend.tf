# This bucket holds all infrastructure states so it should be created before apply
# Can be imported in state later
terraform {
  backend "s3" {
    bucket = "challenge-terraform-states"
    key    = "django.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" {
  region  = "${var.region}"
  version = "~> 1.26"
}
