#!/bin/bash

SLEEP=150

# Set ECS cluster name
echo "ECS_CLUSTER=${ecs_cluster_name}" > /etc/ecs/ecs.config

# instance needs access to internet in order to execute cloud-init properly
# As terraform modules do not support depends_on
# depends_on = ["module.vpc.aws_route.private_nat_gateway"]
# i added sleep here

echo "Sleeping $SLEEP"
sleep $SLEEP

# Install dependecnies
yum install -y git

git clone https://bitbucket.org/kshcherban/django-challenge
cd django-challenge/jenkins

docker build -t jenkins-master .
mkdir -p /opt/jenkins
chmod 1777 /opt/jenkins

# As i use jenkins master as builder i need to allow it docker access
docker run \
    --name jenkins \
    --restart unless-stopped \
    -v /opt/jenkins:/var/jenkins_home \
    -v /var/run/docker.sock:/var/run/docker.sock \
    --network host \
    -d jenkins-master
