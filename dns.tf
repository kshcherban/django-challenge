resource "aws_route53_zone" "private" {
  name   = "${var.internal_dns_zone}"
  vpc_id = "${module.vpc.vpc_id}"

  comment = "internal DNS zone"

  tags {
    Name      = "${var.internal_dns_zone}"
    Terrafrom = true
  }
}

resource "aws_route53_record" "bastion" {
  zone_id = "${aws_route53_zone.private.zone_id}"
  name    = "bastion"
  type    = "A"
  ttl     = "300"
  records = ["${module.ec2_bastion.private_ip[0]}"]
}

resource "aws_route53_record" "jenkins" {
  zone_id = "${aws_route53_zone.private.zone_id}"
  name    = "jenkins"
  type    = "A"
  ttl     = "300"
  records = ["${module.ec2_jenkins.private_ip[0]}"]
}

resource "aws_route53_record" "django" {
  zone_id = "${aws_route53_zone.private.zone_id}"
  name    = "app"
  type    = "A"

  alias {
    name                   = "${aws_lb.django.dns_name}"
    zone_id                = "${aws_lb.django.zone_id}"
    evaluate_target_health = true
  }
}
