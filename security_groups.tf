resource "aws_security_group" "ssh" {
  vpc_id      = "${module.vpc.vpc_id}"
  name        = "ssh"
  description = "Allow incoming ssh connections"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name      = "ssh"
    region    = "${var.region}"
    Terraform = "true"
  }
}

resource "aws_security_group" "web_internal" {
  vpc_id      = "${module.vpc.vpc_id}"
  name        = "web_internal"
  description = "Allow web access from self"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    self      = true
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    self      = true
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    self      = true
  }

  tags {
    Name      = "web_internal"
    region    = "${var.region}"
    Terraform = "true"
  }
}

output "sg_web_id" {
  value = "${aws_security_group.web_internal.id}"
}
