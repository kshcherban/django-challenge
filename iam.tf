resource "aws_iam_instance_profile" "ecs_instance" {
  name = "ecs_instance"
  role = "${aws_iam_role.ecs_instance.name}"
}

resource "aws_iam_role" "ecs_instance" {
  name = "ecs_instance"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "ec2.amazonaws.com"
        ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# This bucket was precreated to store terraform states
resource "aws_iam_policy" "manage_tf_states" {
  name        = "ecs_instance_terraform_s3"
  description = "write access to S3 terraform bucket"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListBucket",
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::challenge-terraform-states",
                "arn:aws:s3:::challenge-terraform-states/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:PutObject"
            ],
            "Resource": [
                "arn:aws:s3:::challenge-terraform-states/apps/*"
            ]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_instance_tf" {
  role       = "${aws_iam_role.ecs_instance.name}"
  policy_arn = "${aws_iam_policy.manage_tf_states.arn}"
}

# Custom policies should be created, these are just used for brevity
resource "aws_iam_role_policy_attachment" "ecs_instance_ecs" {
  role       = "${aws_iam_role.ecs_instance.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerServiceFullAccess"
}

resource "aws_iam_role_policy_attachment" "ecs_instance_ecr" {
  role       = "${aws_iam_role.ecs_instance.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryFullAccess"
}
