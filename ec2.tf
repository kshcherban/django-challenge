resource "aws_key_pair" "default" {
  key_name   = "default-key"
  public_key = "${var.ssh_key}"
}

data "aws_ami" "amazon_linux" {
  most_recent = true

  filter {
    name = "name"

    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

data "aws_ami" "amazon_linux_ecs" {
  most_recent = true

  filter {
    name = "name"

    values = [
      "amzn-ami-*.a-amazon-ecs-optimized",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

module "ec2_bastion" {
  source = "github.com/terraform-aws-modules/terraform-aws-ec2-instance?ref=v1.9.0"

  name           = "bastion"
  instance_count = 1

  ami           = "${data.aws_ami.amazon_linux.id}"
  instance_type = "t2.micro"
  key_name      = "${aws_key_pair.default.key_name}"

  vpc_security_group_ids = [
    "${aws_security_group.ssh.id}",
    "${aws_security_group.web_internal.id}",
  ]

  subnet_id                   = "${module.vpc.public_subnets[0]}"
  associate_public_ip_address = true

  tags = {
    Name      = "bastion"
    Terraform = "true"
  }
}

output "ec2_bastion_public_ip" {
  value = "${module.ec2_bastion.public_ip[0]}"
}

data "template_file" "jenkins_script" {
  template = "${file("${path.module}/user-data/jenkins.sh")}"

  vars {
    ecs_cluster_name = "${aws_ecs_cluster.app.id}"
  }
}

data "template_cloudinit_config" "jenkins" {
  gzip          = false
  base64_encode = false

  part {
    content_type = "text/x-shellscript"
    content      = "${data.template_file.jenkins_script.rendered}"
  }
}

# For brevity i set up jenkins instance as ECS cluster
# But that can be done inside autoscaling group with EFS mounted there for persistent storage
module "ec2_jenkins" {
  source = "github.com/terraform-aws-modules/terraform-aws-ec2-instance?ref=v1.9.0"

  name           = "jenkins"
  instance_count = 1

  ami           = "${data.aws_ami.amazon_linux_ecs.id}"
  instance_type = "t2.micro"
  key_name      = "${aws_key_pair.default.key_name}"

  vpc_security_group_ids = [
    "${aws_security_group.ssh.id}",
    "${aws_security_group.web_internal.id}",
  ]

  # jenkins instance will be a ECS cluster instance that will deploy application
  iam_instance_profile = "${aws_iam_instance_profile.ecs_instance.name}"

  subnet_id = "${module.vpc.private_subnets[0]}"
  user_data = "${data.template_cloudinit_config.jenkins.rendered}"

  tags = {
    Name      = "jenkins"
    Terraform = "true"
  }
}

output "ec2_jenkins_private_ip" {
  value = "${module.ec2_jenkins.private_ip[0]}"
}
