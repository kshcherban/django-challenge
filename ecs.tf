resource "aws_ecs_cluster" "app" {
  name = "django"
}

resource "aws_ecr_repository" "django" {
  name = "django"
}

output "ecs_cluster" {
  value = "${aws_ecs_cluster.app.id}"
}

output "ecr_django" {
  value = "${aws_ecr_repository.django.repository_url}"
}
