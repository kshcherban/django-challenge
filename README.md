This contains all infrastructure code to rollout the deployment.
For this you need to configure aws cli default profile with your AWS access keys,
install aws cli and terraform.

Directory structure:

  * `jenkins` - code of jenkins automation
  * `django` - simple django hello world with terraform code for deployment
  * `cert,user-data` - used by terraform to deploy AWS cluster


1. Install awscli

        pip install --user awscli

1. Configure aws cli

        aws configure

    Please read following doc for more details:
    https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html

1. Install terraform from https://www.terraform.io/downloads.html

1. Create s3 bucket for terraform state persistence (already created)

        aws s3 mb s3://challenge-terraform-states --region eu-central-1

    This step is optional as you can store all states locally, it's just a good practice to keep states remotely.

1. As network is supposed to be private in order to acccess application
please put you **public** ssh key in `terraform.tfvars` in `ssh_key`.

1. Create infrastructure

        terraform init
        terraform apply

Now create SSH tunnel through bastion host to access private VPC,
you will see {{ec2_bastion_public_ip}} in terraform outputs.
```
ssh -D 1080 -l ec2-user {{ec2_bastion_public_ip}}
```
Alternatively i could set VPN tunnel but it required more efforts so i decided to
not spend time.

Configure socks proxy `localhost:1080` in your browser.
Example for Google Chrome
```
google-chrome --proxy-server="socks5://localhost:1080"
```

Application code and deployment pipeline is at: https://bitbucket.org/kshcherban/django-challenge/

Jenkins will be accessible at: http://jenkins.django.local

username: admin  
password: admin  

Run http://jenkins.django.local/job/deploy_pipeline/ job there to deploy application.

Application URL will be available at https://app.django.local
