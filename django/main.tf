terraform {
  backend "s3" {
    bucket = "challenge-terraform-states"
    key    = "apps/django.tfstate"
    region = "eu-central-1"
  }
}

provider "aws" {
  region  = "eu-central-1"
  version = "~> 1.26"
}

data "terraform_remote_state" "main" {
  backend = "s3"

  config {
    region   = "eu-central-1"
    bucket   = "challenge-terraform-states"
    key      = "django.tfstate"
  }
}

# Set via jenkins
variable "version" {}

resource "aws_ecs_task_definition" "django" {
  family       = "django"
  network_mode = "awsvpc"

  container_definitions = <<DEFINITION
[
  {
    "cpu": 256,
    "memory": 512,
    "image": "${data.terraform_remote_state.main.ecr_django}:${var.version}",
    "name": "django",
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80
      }
    ]
  }
]
DEFINITION
}

resource "aws_ecs_service" "django" {
  name            = "django"
  cluster         = "${data.terraform_remote_state.main.ecs_cluster}"
  task_definition = "${aws_ecs_task_definition.django.arn}"
  desired_count   = 1

  network_configuration {
    security_groups = ["${data.terraform_remote_state.main.sg_web_id}"]
    subnets         = ["${data.terraform_remote_state.main.private_subnets_id}"]
  }

  load_balancer {
    target_group_arn = "${data.terraform_remote_state.main.lb_target_group_django}"
    container_name   = "django"
    container_port   = 80
  }
}
