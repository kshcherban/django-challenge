resource "aws_lb" "django" {
  name               = "django"
  internal           = true
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.web_internal.id}"]
  subnets            = ["${module.vpc.private_subnets}"]

  tags {
    Terraform = "true"
  }
}

resource "aws_lb_target_group" "django" {
  name        = "django"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = "${module.vpc.vpc_id}"
  target_type = "ip"
}

output "lb_target_group_django" {
  value = "${aws_lb_target_group.django.id}"
}

resource "aws_lb_listener" "http-django" {
  load_balancer_arn = "${aws_lb.django.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.django.arn}"
    type             = "forward"
  }
}

# Private CA creation requires manual steps in terraform, let's just use quick hack
# to create certificate and attach it to ALB
# Certificate name will be app.django.local
resource "null_resource" "certificate" {
  provisioner "local-exec" {
    command = "openssl req -x509 -newkey rsa:4096 -subj '/CN=app.${var.internal_dns_zone}' -keyout ./cert/key.pem -out ./cert/cert.pem -days 365 -nodes"
  }
}

resource "aws_iam_server_certificate" "django" {
  name             = "django_cert"
  certificate_body = "${file("${path.module}/cert/cert.pem")}"
  private_key      = "${file("${path.module}/cert/key.pem")}"
  depends_on       = ["null_resource.certificate"]

  # ignore change to certificate once uploaded
  lifecycle {
    ignore_changes = ["certificate_body", "private_key"]
  }
}

resource "aws_lb_listener" "https-django" {
  load_balancer_arn = "${aws_lb.django.arn}"
  port              = "443"
  protocol          = "HTTPS"

  # https://docs.aws.amazon.com/elasticloadbalancing/latest/application/create-https-listener.html#describe-ssl-policies
  ssl_policy      = "ELBSecurityPolicy-2016-08"
  certificate_arn = "${aws_iam_server_certificate.django.arn}"

  default_action {
    target_group_arn = "${aws_lb_target_group.django.arn}"
    type             = "forward"
  }
}
