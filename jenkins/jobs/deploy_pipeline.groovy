pipelineJob("deploy_pipeline") {

    description("Deploy django app")

    definition {
        cpsScm {
            scm {
                git {
                    remote {
                        url('https://bitbucket.org/kshcherban/django-challenge')
                        branch('master')
                    }
                }
            }
            scriptPath('django/Jenkinsfile')
        }
    }
}
